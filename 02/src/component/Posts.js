import axios from 'axios';
import React, { useEffect, useState } from 'react';
import Post from './Post';

const Posts = () => {
    const [data, setData] = useState([]);
    const [searchTitle, setSearchTitle] = useState("");
    const [searchId, setSearchId] = useState("");

    useEffect( () => {
        axios.get("https://jsonplaceholder.typicode.com/posts")
            .then((res) => setData(res.data));
    }, []);

    return (
        <div className='posts'>
            <input className='search'  type="text"
            placeholder='entrer titre du post'
            onChange={ (e)=>setSearchTitle(e.target.value) }
             />
             <input className='search' type="number" min='0' max='9' onwheel="return false"

             placeholder="entrer id des auteurs"
             onChange={ (e)=>setSearchId(e.target.value) }

             />

            <div>
            {
                data
                .filter( (post)=> post.userId.toString().includes(searchId))
                .filter( (post)=> post.title.toLowerCase().includes(searchTitle.toLowerCase() ) )
                .map( (post) => <Post post={post} key={post.id}/> )
            }
            </div>
        </div>
    );
};

export default Posts;