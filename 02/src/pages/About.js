import React from 'react';
import Logo from '../component/Logo';
import Navigation from '../component/Navigation';

const About = () => {
    return (
        <div>
            <Logo />
            <Navigation />
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Expedita eum in nisi laudantium officiis earum sapiente incidunt facilis culpa autem, saepe a? Est, eaque. Dolore, labore corporis dolores animi magni molestiae nam soluta blanditiis, ipsum doloremque sunt hic sed temporibus provident deleniti. Enim ipsa similique provident minima iure itaque dolores ad expedita deserunt nostrum reiciendis assumenda asperiores libero, officia voluptas error. Dolorum nesciunt tempora tempore dicta quis. Nihil sed voluptatum quos, ullam asperiores facilis nisi sint, dicta quasi incidunt nesciunt saepe illo molestias exercitationem aperiam recusandae, quod accusamus similique totam quidem. Autem culpa iste distinctio temporibus velit, minima exercitationem perspiciatis? Odio, fugiat molestias perspiciatis accusantium necessitatibus vel temporibus qui, illo fugit a nihil et quod blanditiis laborum ad aliquid. Amet perferendis similique, provident iure odit quidem explicabo corporis, excepturi praesentium, animi reiciendis ratione non molestias distinctio adipisci. Officia quis facere ad ea provident modi ipsa dolore vel obcaecati tempore facilis, debitis unde amet reiciendis temporibus dolor quae recusandae voluptatem ipsam vero. Modi tempora assumenda, inventore voluptate, asperiores voluptas error officiis delectus, quisquam reprehenderit neque consequatur eveniet? Nesciunt, consequuntur labore iste veritatis nobis architecto, asperiores earum est, omnis nam aut ipsam. Fuga distinctio ipsa modi eius beatae aliquid facilis! Aspernatur placeat eveniet itaque nemo numquam vel illum, maxime voluptatem deleniti aut id impedit corrupti modi possimus assumenda dolorum suscipit, explicabo autem perspiciatis? Vitae fuga laborum ex eum? Facilis, blanditiis animi aperiam quos unde voluptatum consectetur aliquam vitae quo laborum neque optio qui quaerat minus hic, vero ad ducimus doloremque fugiat reprehenderit?</p>
        </div>
    );
};

export default About;