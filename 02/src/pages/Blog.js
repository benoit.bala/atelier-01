import React from 'react';
import Logo from '../component/Logo';
import Navigation from '../component/Navigation';
import Posts from '../component/Posts';

const Blog = () => {
    return (
        <div>
            <Logo />
            <Navigation />
            <Posts />
        </div>
    );
};

export default Blog;