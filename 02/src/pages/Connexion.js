import React from 'react';
import Logo from '../component/Logo';
import Navigation from '../component/Navigation';

const Connexion = () => {
    return (
        <div>
            <Logo />
            <Navigation />
        </div>
    );
};

export default Connexion;