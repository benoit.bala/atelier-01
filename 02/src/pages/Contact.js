import React from 'react';
import Logo from '../component/Logo';
import Navigation from '../component/Navigation';

const Contact = () => {

    const names = ['James', 'John', 'Paul', 'Rambo', 'George'];

    const people = [
        {
            name: 'James',
            age: 31,
        },
        {
            name: 'John',
            age: 45,
        },
        {
            name: 'Paul',
            age: 65,
        },
        {
            name: 'Rambo',
            age: 131,
        },
        {
            name: 'George',
            age: 75,
        }
    ];

    return (
        <div>
            <Logo />
            <Navigation />
            <h2>Tableau filtré</h2>
            <ul>
                {
                    names
                        .filter((name) => name.includes('J')).map(
                            (filterName) => (
                                <li>
                                    {filterName}
                                </li>
                            )
                        )
                }
            </ul>
                <h2>Objet filtré</h2>
            <ul>
                {

                    people
                    .filter( (person)=> person.age > 60 )
                    .map(
                        (filtredPerson) => (<li> {filtredPerson.name} </li>)
                    )
                }
            </ul>

        </div>
    );
};

export default Contact;