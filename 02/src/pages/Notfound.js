import React from 'react';
import Logo from '../component/Logo';
import Navigation from '../component/Navigation';

const Notfound = () => {
    return (
        <div>
            <Logo />
            <Navigation />
            <h1>Page non trouvée !</h1>
        </div>
    );
};

export default Notfound;