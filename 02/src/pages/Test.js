import React, { useState } from 'react';
import Logo from '../component/Logo';
import Navigation from '../component/Navigation';
import Voiture from '../component/Voiture';

const Test = () => {
    const [plagePrix, setPlagePrix] = useState(50000);
    const objVoitures = [
        {
            model: "BMW",
            prix: 50000
        },
        {
            model: "Peugeot",
            prix: 20000
        },
        {
            model: "Citroen",
            prix: 25000
        },
        {
            model: "Renault",
            prix: 20000
        },
        {
            model: "Mercedes",
            prix: 80000
        },
        {
            model: "Hyundai",
            prix: 60000
        },
        {
            model: "Porshe",
            prix: 90000
        },
        {
            model: "Maserati",
            prix: 100000
        },
        {
            model: "Volkswagen",
            prix: 27000
        },
        {
            model: "Toyota",
            prix: 15000
        },
        {
            model: "Fiat",
            prix: 22000
        },
        {
            model: "Ford",
            prix: 37000
        },
        {
            model: "Audi",
            prix: 75000
        },
        {
            model: "Abarth",
            prix: 90000
        },
        {
            model: "Alfa Romeo",
            prix: 58000
        },
        {
            model: "Bentley",
            prix: 80000
        },
        {
            model: "Chevrolet",
            prix: 85000
        },
        {
            model: "Dodge",
            prix: 77000
        },
        {
            model: "Dacia",
            prix: 7000
        },
        {
            model: "Nissan",
            prix: 62000
        },


    ];
    return (
        <div>
            <Logo />
            <Navigation />
            <div className='voitures'>
                <ul>
                    {
                        objVoitures
                            .filter((voiture) => voiture.prix < plagePrix)
                            .map((elmt,indice) => <Voiture param={elmt} indice={indice} />)
                    }
                </ul>
            </div>
            <div className='filtre'>
                <input
                    type="range"
                    min="0"
                    max="500000"
                    defaultValue={plagePrix}
                    onChange={(e) => setPlagePrix(e.target.value)}
                />
            </div>
        </div>

    );
};

export default Test;